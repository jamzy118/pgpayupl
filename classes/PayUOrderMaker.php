<?php namespace Keios\PGPayUPL\Classes;

use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\Core\Operator;
use OpenPayU_Configuration;

/**
 * Class PayUOrderMaker
 *
 * @package Keios\PGPayUPL
 */
class PayUOrderMaker
{
    protected $cart;

    protected $details;

    protected $payment;

    public function __construct(Operator $payment, Orderable $cart, Details $details)
    {
        $this->cart = $cart;
        $this->details = $details;
        $this->payment = $payment;
    }

    public function make()
    {
        $order = [];

        $this->addUrls($order);
        $this->addGeneralData($order);
        $this->addProducts($order);
        $this->addBuyer($order);

        return $order;
    }

    protected function addProducts(array &$order)
    {
        $index = 0;

        /**
         * @var \Keios\PaymentGateway\ValueObjects\Item $item
         */
        foreach ($this->cart as $item) {
            $order['products'][$index]['name'] = $item->getDescription();
            $order['products'][$index]['unitPrice'] = $item->getSingleGrossPrice()->getAmount();
            $order['products'][$index]['quantity'] = $item->getCount();
            $index++;
        }
    }

    protected function addGeneralData(array &$order)
    {
        $chargeAmount = $this->cart->getTotalGrossCost(true);

        $order['customerIp'] = \Request::getClientIp();
        $order['merchantPosId'] = OpenPayU_Configuration::getMerchantPosId();
        $order['description'] = $this->details->getDescription();
        $order['currencyCode'] = strtoupper($chargeAmount->getCurrency()->getIsoCode());
        $order['totalAmount'] = $chargeAmount->getAmount();
        $order['extOrderId'] = $this->payment->uuid;
    }

    protected function addUrls(array &$order)
    {
        $order['notifyUrl'] = \URL::to('_paymentgateway/'.OperatorUrlizer::urlize($this->payment));
        $order['continueUrl'] = \URL::to($this->payment->returnUrl);
    }

    protected function addBuyer(array &$order)
    {
        $order['buyer']['email'] = $this->details->getEmail();
        $order['buyer']['firstName'] = $this->details->get('first_name');
        $order['buyer']['lastName'] = $this->details->get('last_name');
    }
}