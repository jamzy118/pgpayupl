<?php namespace Keios\PGPayUPL;

use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use System\Classes\PluginBase;
use Event;

/**
 * PG-PayU-PL Plugin Information File
 *
 * @package Keios\PGPayUPL
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PG-PayU-PL',
            'description' => 'keios.pgpayupl::lang.labels.pluginDesc',
            'author'      => 'Keios',
            'icon'        => 'icon-dollar'
        ];
    }

    public function register()
    {
        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGPayUPL\Operators\PayU');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'payu.general'         => [
                            'label' => 'keios.pgpayupl::lang.settings.general',
                            'tab'   => 'keios.pgpayupl::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'payu.info'            => [
                            'type' => 'partial',
                            'path' => '$/keios/pgpayupl/partials/_payu_info.htm',
                            'tab'  => 'keios.pgpayupl::lang.settings.tab',
                        ],
                        'payu.pl.posId'        => [
                            'label'   => 'keios.pgpayupl::lang.settings.posId',
                            'tab'     => 'keios.pgpayupl::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'payu.pl.signatureKey' => [
                            'label'   => 'keios.pgpayupl::lang.settings.signatureKey',
                            'tab'     => 'keios.pgpayupl::lang.settings.tab',
                            'type'    => 'text',
                            'default' => ''
                        ],
                        'payu.testMode'        => [
                            'label'      => 'keios.pgpayupl::lang.settings.testMode',
                            'tab'        => 'keios.pgpayupl::lang.settings.tab',
                            'type'       => 'switch'
                        ],
                    ]
                );
            }
        );
    }
}
