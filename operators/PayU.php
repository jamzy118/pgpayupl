<?php namespace Keios\PGPayUPL\Operators;

use Finite\StatefulInterface;
use Keios\PaymentGateway\Exceptions\CancellationFailureException;
use Keios\PaymentGateway\Exceptions\AcceptanceFailureException;
use Keios\PaymentGateway\Exceptions\RefundFailureException;
use Keios\PaymentGateway\Exceptions\RejectionFailureException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Symfony\Component\HttpFoundation\Response;
use Keios\PGPayUPL\Classes\PayUOrderMaker;
use Keios\PaymentGateway\Core\Operator;
use OpenPayU_Configuration;
use OpenPayU_Refund;
use OpenPayU_Order;


/**
 * Class PayU
 *
 * @package Keios\PGPayUPL
 */
class PayU extends Operator implements StatefulInterface
{
    use SettingsDependent;

    const CREDIT_CARD_REQUIRED = false;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgpayupl::lang.operators.payupl';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgpayupl/assets/img/payu.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {
        $this->setAuthorization();

        $orderMaker = new PayUOrderMaker($this, $this->cart, $this->paymentDetails);

        $order = $orderMaker->make();

        try {
            /**
             * @var \OpenPayU_Result $result
             */
            $result = OpenPayU_Order::create($order);
        } catch (\Exception $ex) {
            return new PaymentResponse($this, null, [$ex->getMessage()]);
        }

        $this->payUOrderId = $result->getResponse()->orderId;

        return new PaymentResponse($this, $result->getResponse()->redirectUri); // wtf payu? @return string? w docs object?
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function processNotification(array $data)
    {
        $this->setAuthorization();

        $request = \Request::instance();
        $content = $request->getContent();
        $data = stripslashes(trim($content));


        $parsedNotification = OpenPayU_Order::consumeNotification($data);

        if ($parsedNotification->getResponse()->order) {
            $notification['type'] = 'status_update';
            $notification['status'] = $parsedNotification->getResponse()->order->status; //NEW PENDING CANCELLED REJECTED COMPLETED WAITING_FOR_CONFIRMATION
        } elseif ($parsedNotification->getResponse()->refund) {
            $notification['type'] = 'refund_update';
            $notification['status'] = $parsedNotification->getResponse()->refund->status;
        } else {
            return new Response('Unknown notification type.', 400);
        }

        if ($notification['type'] === 'status_update') {
            try {
                switch ($notification['status']) {
                    case 'NEW':
                        // nothing new
                        break;
                    case 'PENDING':
                        // ok, gets us nowhere, but ok
                        break;
                    case 'CANCELED':
                        $this->cancel();
                        break;
                    case 'REJECTED':
                        $this->rejectFromApi();
                        break;
                    case 'COMPLETED':
                        $this->accept();
                        break;
                    case 'WAITING_FOR_CONFIRMATION':
                        // ok, waiting
                        break;
                }
            } catch (AcceptanceFailureException $afex) {
                return new Response('Already accepted.', 500);
            } catch (CancellationFailureException $cfex) {
                return new Response('Already cancelled.', 500);
            } catch (RejectionFailureException $rfex) {
                return new Response('Already rejected.', 500);
            } catch (\Exception $ex) {
                return new Response('Internal server error.', 500);
            }
        } elseif ($notification['type'] === 'refund_update') {
            if ($notification['status'] === 'FINALIZED') {
                try {
                    $this->refundFromApi();
                } catch (RefundFailureException $rfex) {
                    return new Response('Already refunded.', 500);
                }
            }
        }

        return new Response('OK', 200);
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        $this->setAuthorization();

        try {
            $refund = OpenPayU_Refund::create(
                $this->payUOrderId,
                'Zwrot kosztów za '.$this->description
            );
        } catch (\Exception $ex) {
            return new PaymentResponse($this, null, [$ex->getMessage()]);
        }

        return new PaymentResponse($this, null);
    }

    /**
     * @param array $data
     *
     * @return integer
     * @throws \Exception
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['order']) && isset($data['order']['extOrderId'])) {
            return $data['order']['extOrderId'];
        } else {
            throw new \Exception('Invalid payment data - uuid was not found.');
        }
    }

    protected function setAuthorization()
    {
        $this->getSettings();

        OpenPayU_Configuration::setEnvironment('secure');

        if ($this->settings->get('payu.testMode')) {
            OpenPayU_Configuration::setMerchantPosId(145227);
            OpenPayU_Configuration::setSignatureKey('13a980d4f851f3d9a1cfc792fb1f5e50');
        } else {
            OpenPayU_Configuration::setMerchantPosId($this->settings->get('payu.pl.posId'));
            OpenPayU_Configuration::setSignatureKey($this->settings->get('payu.pl.signatureKey'));
        }
    }


}